import subprocess
import re
import argparse
import shutil
import sys
from threading import Thread

from PyQt6 import QtWidgets

from ui.main_widget import Ui_MainWindow
from drive import Drive


class SDCloner:
    def __init__(self, ignore, directory):
        self.app = None
        self.drives = []
        self.done = []
        self.copying = []
        self.do_run = True
        self.ignore = [] if ignore is None else ignore
        self.ignore = [element.upper() for element in self.ignore]
        self.directory = directory

    # https://stackoverflow.com/a/12673589
    def get_drives(self):
        drivelist = subprocess.Popen('wmic logicaldisk get access,name,drivetype', shell=True, stdout=subprocess.PIPE)
        drivelisto, err = drivelist.communicate()
        drivelisto = drivelisto.decode("utf-8")
        m = re.findall(r'0\s+2\s+([A-Z]):', drivelisto)  # Look for letters of removable drives
        for drive in m:
            if drive in self.drives:
                pass
            if drive.upper() not in self.ignore:
                self.drives.append(Drive(drive))

    def copy(self, target):
        self.copying.append(target)
        try:
            shutil.copytree(self.directory, target + ":\\", dirs_exist_ok=True)
        except Exception as e:
            print(e)
        self.done.append(target)
        print(f"Copy done on {target}")

    def run_headless(self):
        try:
            while True:
                self.get_drives()
                for drive in self.drives:
                    if drive not in self.copying:
                        print(f"Copying on {drive}")
                        Thread(target=self.copy, args=drive).start()
                for drive in self.done:
                    if drive not in self.drives:
                        print(f"{drive} removed")
                        self.copying.remove(drive)
                        self.done.remove(drive)
        except KeyboardInterrupt:
            print("Quitting")
        except Exception as e:
            print("Unexpected exception: ", e)

    def run_ui(self):
        self.app = QtWidgets.QApplication(sys.argv)
        self.mainWindow = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self.mainWindow)
        self.mainWindow.show()

        sys.exit(self.app.exec())


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--ignore', type=str, nargs='+', help='drives to ignore')
    parser.add_argument('-d', '--directory', type=str, help='directory to copy to drives')
    parser.add_argument('-c', '--cli', dest='headless', action="store_true")
    parser.set_defaults(headless=False)
    args = parser.parse_args()
    sc = SDCloner(args.ignore, args.directory)
    if args.headless:
        sc.run_headless()
    else:
        sc.run_ui()
