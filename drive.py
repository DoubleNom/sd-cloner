import shutil
from threading import Thread


class Drive:
    def __init__(self, letter):
        self.letter = letter.upper()
        self._copying = False
        self._done = False

    @property
    def status(self):
        if self._done:
            return "Done"
        elif self._copying:
            return "Copying"
        else:
            return "Waiting"

    def __eq__(self, other):
        if isinstance(other, str):
            return self.letter == other.upper()
        elif isinstance(other, Drive):
            return self.letter == other.letter
        else:
            return False

    def _copy(self, target):
        try:
            shutil.copytree(target, self.letter + ":\\", dirs_exist_ok=True)
        except Exception as e:
            print(e)
        self._done = True

    def do_copy(self, target):
        if self._copying:
            return

        self._copying = True
        Thread(target=self._copy, args=target).start()
